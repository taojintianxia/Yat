-- @testpoint: 表达式做为参数的存储过程测试——聚集函数-SUM()

drop table if exists FVT_PROC_EXPR_PARAM_table_025;
CREATE TABLE FVT_PROC_EXPR_PARAM_table_025(
  T1 INT,
  T2 INTEGER NOT NULL,
  T3 BIGINT,
  T4 NUMBER DEFAULT 0.2332,
  T5 NUMBER(12,2),
  T6 NUMBER(12,6),
  T7 BINARY_DOUBLE,
  T8 DECIMAL,
  T9 DECIMAL(8,2),
  T10 DECIMAL(8,4),
  T11 REAL,
  T12 CHAR(4000),
  T13 CHAR(100),
  T14 VARCHAR(4000),
  T15 VARCHAR(100),
  T16 VARCHAR2(4000),
  T17 NUMERIC,
  T19 DATE,
  T20 TIMESTAMP,
  T21 TIMESTAMP(6),
  T22 BOOL
) ;

create unique index  FVT_PROC_EXPR_PARAM_table_index_025 on FVT_PROC_EXPR_PARAM_table_025(T1);
create index FVT_PROC_EXPR_PARAM_table_index1_025 on FVT_PROC_EXPR_PARAM_table_025(T2,T17,T20);


--表达式做为参数的存储过程测试——函数表达式-聚集函数-SUM()
--创建存储过程
CREATE OR REPLACE PROCEDURE FVT_PROC_EXPR_PARAM_025(P1 OUT NUMBER)  AS
V_num BIGINT;
BEGIN
select SUM(T3) INTO V_num from FVT_PROC_EXPR_PARAM_table_025;
P1 :=V_num;
raise info'P1=:%',P1;
EXCEPTION
WHEN NO_DATA_FOUND THEN raise info 'NO_DATA_FOUND';
END;
/
--调用存储过程
DECLARE
V1 NUMBER;
BEGIN
FVT_PROC_EXPR_PARAM_025(V1);
END;
/
--清理环境
DROP PROCEDURE FVT_PROC_EXPR_PARAM_025;
DROP TABLE IF EXISTS FVT_PROC_EXPR_PARAM_TABLE_025;

